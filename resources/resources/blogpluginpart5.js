Plugin.BlogPluginPartFive = class MyPlugin {
    constructor(pluginService, eventBus) {
        this.pluginService = pluginService;
        this.eventBus = eventBus;

        this.init();
    }

    init() {
        // subscribe to the cco eventbus
        this.eventBus.subscribe({
            'handleEvent': (event) => {
                // retrieve the receipt store to access the receipt model so we are able
                // to check if a customer was added to the receipt or not.
                this.receiptStore = this.pluginService.getContextInstance('ReceiptStore');
                // retrieve the user store from cco context
                this.userStore = this.pluginService.getContextInstance('UserStore');
                // get the translation store
                this.translationStore = this.pluginService.getContextInstance('TranslationStore');
                // get the current user so the translationStore knows which language to translate
                this.user = this.userStore.getUser();
                switch (event.getType()) {
                    case 'SHOW_ADD_INFO':
                        // if the receiptModel is empty there is no open receipt in the ui
                        if (this.receiptStore.receiptModel === null) {
                            // shows a message box to the cashier to inform that additional info can only
                            // be shown with an open receipt
                            this.eventBus.push('SHOW_MESSAGE_BOX', {
                                'title': this.translationStore.getTranslation('NO_OPEN_RECEIPT', this.user.getLanguageCode()),
                                'message': this.translationStore.getTranslation('NO_OPEN_RECEIPT', this.user.getLanguageCode())
                            });
                            return;
                        }

                        if (this.receiptStore.receiptModel.businessPartner === null) {
                            // shows a message box to the cashier to inform that additional info can only
                            // be shown when a customer is selected.
                            this.eventBus.push('SHOW_MESSAGE_BOX', {
                                'title': this.translationStore.getTranslation('NO_CUSTOMER_IN_RECEIPT', this.user.getLanguageCode()),
                                'message': this.translationStore.getTranslation('NO_CUSTOMER_IN_RECEIPT', this.user.getLanguageCode())
                            });
                            return;
                        }

                        // get the externalID from the businesspartner in the receiptModel
                        let customerId = this.receiptStore.receiptModel.businessPartner.externalID;
                        // send an event back to our java coding where we use our Dao layer to retrieve the data.
                        this.pluginService.backendPluginEvent('GET_ADDITIONAL_INFO_FOR_CUSTOMER', {
                            'customerId': customerId
                        });
                        break;
                    case 'SHOW_ADD_FORM':
                        this.buildElements();
                        // check if the backend send a record from the database.
                        if (Object.keys(event.getPayload()).length !== 0) {
                            // set the values we got from the backend to our datamodels
                            this.model1.value = event.getPayload().addFld1;
                            this.modelAmount.value = event.getPayload().addFld2;
                            this.modelDecimal.value = event.getPayload().addFld3;
                            this.selectModel.setSelectedOptionKey(event.getPayload().addFld4);
                        }

                        // in the validate function we can implement checks to have mandatory field checks etc.
                        cco.GridLayoutComponent.prototype['validate'] = () => {
                            if (this.model1.value === '' || this.model1.value === 'MANDATORY') {
                                this.eventBus.push('SHOW_MESSAGE_BOX', {
                                    'title': this.translationStore.getTranslation('ADD_FLD1_MND', this.user.getLanguageCode()),
                                    'message': this.translationStore.getTranslation('ADD_FLD1_MND', this.user.getLanguageCode())
                                });
                                return false;
                            }
                            return true;
                        };

                        this.ribbonSelectModel = new cco.IndexSelectionModel();
                        this.ribbonSelectModel.setSelected(0);

                        // we are setting an observer to the ribbonSelectModel so we can react
                        // when the cashier clicks on another ribbon
                        this.ribbonSelectModel.addObserver({
                            modelChanged: (inputModel) => {
                                this.dynamicPropsRibbonOne.setAndEmitPropertiesIfChanged({
                                    class: inputModel.getSelected() !== 0 ? 'pluginRibbonHidden' : ''
                                });
                                this.dynamicPropsRibbonTwo.setAndEmitPropertiesIfChanged({
                                    class: inputModel.getSelected() !== 1 ? 'pluginRibbonHidden' : ''
                                });
                            }
                        });

                        // we are also creating something called DynamicProperties so we can hide ui components when not used e.g.
                        // when the cashier uses ribbon the first ribbon so all elements on the second ribbon will not be visible.
                        this.dynamicPropsRibbonOne = new cco.DynamicProperties({
                            class: this.ribbonSelectModel.getSelected() !== 0 ? 'pluginRibbonHidden' : ''
                        });

                        // so we are basically setting a css class when our datamodel behind the ribbons has a certain value.
                        // the css class can be found in our blogpluginpart5.css
                        // With the Dynamic Properties you can set all properties of a component. e.g. changing the text, the height, ...
                        // of a ui component dynamically.
                        this.dynamicPropsRibbonTwo = new cco.DynamicProperties({
                            class: this.ribbonSelectModel.getSelected() !== 1 ? 'pluginRibbonHidden' : ''
                        });

                        // We are pushing an event into ccos event bus to show a generic eventbus.
                        // This eventbus needs a configuration (which contains all ui components the generic input
                        // should show. Furthermore we set some properties like the title and implement the callback
                        // function which is called when the cashier wants to close the form.
                        this.eventBus.push('SHOW_GENERIC_INPUT', {
                            configuration: [
                                this.createRibbonConfiguration(),
                            ],
                            title: 'Additional Info for customer ' + this.receiptStore.receiptModel.businessPartner.externalID,
                            showKeyboardSwitchButton: true,
                            keyboardType: 'keyboard',
                            widthFactor: 0.8,
                            heightFactor: 0.8,
                            callback: (positive) => {
                                if (positive) {
                                    let customerId = this.receiptStore.receiptModel.businessPartner.externalID;
                                    let addFld1 = this.model1.value;
                                    let addFld2 = Math.trunc(this.modelAmount.value);
                                    let addFld3 = this.modelDecimal.value;
                                    let addFld4 = this.selectModel.getSelectedOptionKey();
                                    this.pluginService.backendPluginEvent('SAVE_ADDITIONAL_CUSTOMER_DATA', {
                                        'customerId': customerId,
                                        'addFld1': addFld1,
                                        'addFld2': addFld2,
                                        'addFld3': addFld3,
                                        'addFld4': addFld4
                                    });
                                }
                            }
                        });
                        break;
                }
            }
        });
    }

    createRibbonConfiguration() {
        // define which text the buttons should show
        let buttons = [{
            content: 'Tab1'
        }, {
            content: 'Tab2'
        }];

        // creating a GenericInputConfiguration containing a GridLayoutComponent
        return new cco.GenericInputConfiguration('RibbonForm', '2', 'Component', new cco.InputModel(), null, cco.GridLayoutComponent, {
            props: {
                // set this so elements in the gridlayout can overlap. So elements in ribbon 1 and 2 can have the
                // same position.
                overlappingElements: true,
                elements: [{
                    index: [0, 0],
                    hspan: 25,
                    vspan: 15,
                    content: {
                        // this is the ui element we are using for the ribbons.
                        component: cco.StatefulButtonBarComponent,
                        props: {
                            // setting the buttons as prop and also our datamodel.
                            buttons: buttons,
                            class: 'light',
                            populateModel: true,
                            model: this.ribbonSelectModel
                        }
                    }
                }, {
                    index: [30, 0],
                    hspan: 25,
                    vspan: 10,
                    content: {
                        // our father gridlayout will contain another
                        // gridlayout. One per ribbon.
                        component: cco.GridLayoutComponent,
                        props: {
                            static: {
                                elements: this.ribbonOneElements
                            },
                            // setting the dynamic property to change the css class
                            // based on the selection of the ribbon.
                            dynamic: this.dynamicPropsRibbonOne,
                            cols: 100,
                            rows: 100
                        }
                    }
                }, {
                    index: [30, 0],
                    hspan: 25,
                    vspan: 10,
                    content: {
                        component: cco.GridLayoutComponent,
                        props: {
                            static: {
                                elements: this.ribbonTwoElements
                            },
                            dynamic: this.dynamicPropsRibbonTwo,
                            cols: 100,
                            rows: 100
                        }
                    }
                }],
                cols: 100,
                rows: 100
            }
        });
    }


    buildElements() {
        // all our ui components will be bound to a data model so that we can easily access the
        // values of the cashiers input
        this.model1 = new cco.InputModel();
        this.modelAmount = new cco.InputModel();
        this.modelDecimal = new cco.InputModel();
        // the option model is like a select box so we need to add options to select.
        this.selectModel = new cco.OptionModel();
        this.selectModel.addOption(new cco.Option('option1', 'First option'));
        this.selectModel.addOption(new cco.Option('option2', 'Second option'));
        this.selectModel.addOption(new cco.Option('option3', 'Third option'));
        this.selectModel.selectFirstOption();

        // we are creating 2 arrays each containing all ui components for each ribbon.
        this.ribbonOneElements = [{
            index: [0, 0],
            hspan: 8,
            vspan: 8,
            content: {
                // this is our first LabelComponent
                component: cco.LabelComponent,
                props: {
                    text: 'Additional Field 1:',
                    class: 'tableCellLayout'
                }
            }
        }, {
            index: [0, 10],
            hspan: 20,
            vspan: 8,
            content: {
                // this is our first InputFieldComponent
                component: cco.InputFieldComponent,
                props: {
                    // bound the datamodel
                    id: 'first',
                    model: this.model1,
                    class: 'tableCellLayout',
                    autoSetPrimaryTarget: true
                }
            }
        }, {
            index: [16, 0],
            hspan: 8,
            vspan: 8,
            content: {
                // this is our second LabelComponent
                component: cco.LabelComponent,
                props: {
                    text: 'Additional Field 2:',
                    class: 'tableCellLayout'
                }
            }
        }, {
            index: [16, 10],
            hspan: 20,
            vspan: 8,
            content: {
                component: cco.InputFieldComponent,
                props: {
                    id: 'second',
                    model: this.modelAmount,
                    class: 'tableCellLayout',
                    numeric: true,
                    autoSetPrimaryTarget: true
                }
            }
        }];

        this.ribbonTwoElements = [{
            index: [0, 0],
            hspan: 8,
            vspan: 8,
            content: {
                component: cco.LabelComponent,
                props: {
                    text: 'Additional Field 3:',
                    class: 'tableCellLayout'
                }
            }
        }, {
            index: [0, 10],
            hspan: 20,
            vspan: 8,
            content: {
                component: cco.InputFieldComponent,
                props: {
                    id: 'third',
                    model: this.modelDecimal,
                    numeric: true,
                    class: 'tableCellLayout',
                    autoSetPrimaryTarget: true
                }
            }
        }, {
            index: [16, 0],
            hspan: 8,
            vspan: 8,
            content: {
                component: cco.LabelComponent,
                props: {
                    text: 'Additional Field 4:',
                    class: 'tableCellLayout'
                }
            }
        }, {
            index: [16, 10],
            hspan: 20,
            vspan: 8,
            content: {
                // For our selection model we use the ui component called SelectComponent
                component: cco.SelectComponent,
                props: {
                    model: this.selectModel,
                    class: 'tableCellLayout'
                }
            }
        }];
    }
};
