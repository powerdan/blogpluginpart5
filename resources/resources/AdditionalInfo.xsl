<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:func="http://exslt.org/functions" version="2.0"
                extension-element-prefixes="func">
    <xsl:output method="xml" indent="yes"/>
    <!-- include the user defined functions -->
    <xsl:include href="BaseIncludes.xsl"/>
    <!-- Include Header-Template -->
    <xsl:include href="HeaderTemplateA4.xsl"/>
    <!-- Include Footer-Template -->
    <xsl:include href="FooterTemplateA4.xsl"/>
    <!-- Define the variables for the entry nodes to easier access them -->
    <xsl:variable name="addInfo"
                  select="//entry[string/text()='addInfo']/com.hokona.cco.blogpluginpart5.dto.AdditionalInfoDto"/>


    <xsl:template match="/">
        <fo:root font-family="Arial">
            <fo:layout-master-set>
                <fo:simple-page-master margin-top="1cm" margin-bottom="5mm" margin-left="2cm" margin-right="2cm"
                                       page-width="21.001cm" page-height="29.7cm" master-name="mainpage-layout">
                    <fo:region-body region-name="frame-body" margin-bottom="25mm" margin-top="30mm" margin-left="0mm"
                                    margin-right="0mm"/>
                    <fo:region-before region-name="region-before" display-align="after" extent="23mm"/>
                    <fo:region-after region-name="region-after" display-align="before" extent="23mm"/>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="main-sequence">
                    <fo:repeatable-page-master-reference master-reference="mainpage-layout" page-position="rest"/>
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="main-sequence">
                <fo:title>Invoice PrintOut</fo:title>
                <!-- Header -->
                <fo:static-content flow-name="region-before" font-size="12pt">
                    <xsl:apply-templates select="." mode="header"/>
                    <fo:block line-height="1.17" padding="0.074cm" border-left="none" border-right="none"
                              border-top="none" border-bottom="0.035cm solid #000000" font-size="6pt"/>
                </fo:static-content>
                <!-- Footer -->
                <fo:static-content flow-name="region-after" font-size="12pt">
                    <fo:block line-height="1.17" padding="0.074cm" border-left="none" border-right="none"
                              border-top="none" border-bottom="0.035cm solid #000000" font-size="6pt"
                              font-weight="bold"/>
                    <xsl:apply-templates select="." mode="footer"/>
                </fo:static-content>
                <!-- Main section -->
                <fo:flow flow-name="frame-body">
                    <fo:block line-height="1.17" text-align="start"> </fo:block>
                    <fo:block line-height="1.17" text-align="start"> </fo:block>
                    <fo:block>
                        <fo:table width="17cm" table-layout="fixed" border="0.01cm  solid #000000">
                            <fo:table-column column-width="3.000cm"/>
                            <fo:table-column column-width="4.000cm"/>
                            <fo:table-column column-width="2.000cm"/>
                            <fo:table-column column-width="3.500cm"/>
                            <fo:table-column column-width="4.500cm"/>
                            <fo:table-header>
                                <fo:table-row>
                                    <fo:table-cell background-color="#eeeeee" padding="0.097cm"
                                                   border-left="0.01cm  solid #000000" border-right="none"
                                                   border-top="0.01cm  solid #000000"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" text-align="center" font-size="10pt">
                                                CustomerId
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell background-color="#eeeeee" padding="0.097cm"
                                                   border-left="0.01cm  solid #000000" border-right="none"
                                                   border-top="0.01cm  solid #000000"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" text-align="center" font-size="10pt">
                                                AddFld1
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell background-color="#eeeeee" padding="0.097cm"
                                                   border-left="0.01cm  solid #000000" border-right="none"
                                                   border-top="0.01cm  solid #000000"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" text-align="center" font-size="10pt">
                                                AddFld2
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell background-color="#eeeeee" padding="0.097cm"
                                                   border-left="0.01cm  solid #000000" border-right="none"
                                                   border-top="0.01cm  solid #000000"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" text-align="center" font-size="10pt">
                                                AddFld3
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell background-color="#eeeeee" padding="0.097cm"
                                                   border-left="0.01cm  solid #000000" border-right="none"
                                                   border-top="0.01cm  solid #000000"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" text-align="center" font-size="10pt">
                                                AddFld4
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-header>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell padding="0.097cm" border-left="0.01cm  solid #000000"
                                                   border-right="none" border-top="none"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" margin-top="0cm"
                                                      margin-bottom="0cm" font-size="7pt">
                                                <xsl:value-of
                                                        select="$addInfo/customerId/text()"/>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell padding="0.097cm" border-left="0.01cm  solid #000000"
                                                   border-right="none" border-top="none"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" margin-top="0cm"
                                                      margin-bottom="0cm" font-size="7pt">
                                                <xsl:value-of
                                                        select="$addInfo/addFld1/text()"/>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell padding="0.097cm" border-left="0.01cm  solid #000000"
                                                   border-right="none" border-top="none"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" margin-top="0cm"
                                                      margin-bottom="0cm" font-size="7pt">
                                                <xsl:value-of
                                                        select="$addInfo/addFld2/text()"/>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell padding="0.097cm" border-left="0.01cm  solid #000000"
                                                   border-right="none" border-top="none"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" margin-top="0cm"
                                                      margin-bottom="0cm" font-size="7pt">
                                                <xsl:value-of
                                                        select="$addInfo/addFld3/text()"/>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell padding="0.097cm" border-left="0.01cm  solid #000000"
                                                   border-right="none" border-top="none"
                                                   border-bottom="0.01cm  solid #000000">
                                        <fo:block-container>
                                            <fo:block line-height="1.17" margin-top="0cm"
                                                      margin-bottom="0cm" font-size="7pt">
                                                <xsl:value-of
                                                        select="$addInfo/addFld4/text()"/>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>

                    <fo:block line-height="1.17" margin-top="0cm" margin-bottom="0cm" font-size="8pt"> </fo:block>


                    <fo:block line-height="1.17" margin-top="0cm" margin-bottom="1cm" font-size="8pt"> </fo:block>


                    <fo:block line-height="1.17" text-align="start" font-size="10pt"> </fo:block>


                    <fo:block line-height="1.17" margin-top="0cm" margin-bottom="0.212cm" text-align="start"
                              font-size="10pt"> 
                    </fo:block>
                    <fo:block line-height="1.17" margin-top="0cm" margin-bottom="0.212cm" text-align="start"
                              font-size="10pt"> 
                    </fo:block>


                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

</xsl:stylesheet>
