package com.hokona.cco.blogpluginpart5.print;

import com.hokona.cco.blogpluginpart5.PluginPropertiesConstants;
import com.hokona.cco.blogpluginpart5.dto.AdditionalInfoDto;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.entity.PrintTemplateEntity;
import com.sap.scco.ap.pos.printer.SelectableDataStructure;
import com.sap.scco.ap.pos.printer.printjobbuilder.BasePrintJobBuilder;
import com.sap.scco.util.logging.Logger;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.Map;

/**
 * robertzieschang 18.11.20
 **/
public class AdditionalInfoPrintJobBuilder extends BasePrintJobBuilder {

    private static final Logger log = Logger.getLogger(AdditionalInfoPrintJobBuilder.class);

    private static AdditionalInfoPrintJobBuilder instance;

    public static synchronized AdditionalInfoPrintJobBuilder getInstance() {
        if (instance == null) {
            instance = new AdditionalInfoPrintJobBuilder();
        }
        return instance;
    }

    private AdditionalInfoPrintJobBuilder() {
        super();
    }

    @Override
    protected boolean needsMerchantCopy(PrintTemplateEntity printTemplateEntity, Object o, CDBSession cdbSession, boolean b) {
        return false;
    }

    @Override
    protected boolean isTemplateEnabledForSpecificBuilder(PrintTemplateEntity printTemplateEntity, Object o, CDBSession cdbSession, boolean b) {
        return true;
    }

    @Override
    protected void mergeTemplateWithSpecificData(Map<String, Object> map, PrintTemplateEntity printTemplateEntity, Object o, CDBSession cdbSession, boolean b) throws IOException, TemplateException {
        log.fine("Add data to map");
        AdditionalInfoDto additionalInfoDto = (AdditionalInfoDto) o;

        map.put("addInfo", additionalInfoDto);
    }

    @Override
    protected void addSpecificSelectableDataStructure(SelectableDataStructure selectableDataStructure) {
        SelectableDataStructure additionalInfoDataStructure = new SelectableDataStructure("AdditionalInfo", SelectableDataStructure.TYPE_LIST, "AdditionalInfo");
        this.addSubelementToSelectableStructure(additionalInfoDataStructure, "CustomerId", SelectableDataStructure.TYPE_STRING, "CustomerId");
        this.addSubelementToSelectableStructure(additionalInfoDataStructure, "addFld1", SelectableDataStructure.TYPE_STRING, "AddFld1");
        this.addSubelementToSelectableStructure(additionalInfoDataStructure, "addFld2", SelectableDataStructure.TYPE_INT, "AddFld2");
        this.addSubelementToSelectableStructure(additionalInfoDataStructure, "addFld3", SelectableDataStructure.TYPE_DECIMAL, "AddFld3");
        this.addSubelementToSelectableStructure(additionalInfoDataStructure, "addFld4", SelectableDataStructure.TYPE_STRING, "AddFld4");
        selectableDataStructure.addSubelement("AdditionalInfo", additionalInfoDataStructure);
    }

    private void addSubelementToSelectableStructure(SelectableDataStructure parent, String name, String dataType, String descriptionKey) {
        SelectableDataStructure childElement = new SelectableDataStructure(name, dataType, descriptionKey);
        parent.addSubelement(name, childElement);
    }

    @Override
    public String getDescription() {
        return PluginPropertiesConstants.ADDITIONAL_INFO_PRINT_JOB_BUILDER_DESC;
    }

    @Override
    public boolean isResponsibleForDataSource(Object o) {
        return o instanceof AdditionalInfoDto;
    }
}
