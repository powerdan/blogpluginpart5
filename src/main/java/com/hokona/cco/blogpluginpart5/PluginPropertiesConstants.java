package com.hokona.cco.blogpluginpart5;

/**
 * robertzieschang 12.11.20
 * <p>
 * Final class to hold all constants used by various classes.
 **/
public final class PluginPropertiesConstants {

    private PluginPropertiesConstants() {
        throw new RuntimeException("Service Class");
    }

    // global plugin information strings
    public static final String PLUGIN_ID = "blogpluginpart5";
    public static final String PLUGIN_NAME = "Blog plugin part 5";

    // other statics
    public static final String ADDITIONAL_INFO_PRINT_JOB_BUILDER_DESC = "AdditionalInfoPrintJobBuilder";
    public static final String ADDITIONAL_INFO_PRINT_TEMPLATE = "AdditionalInfo";

    // exceptions
    public static final String EXCEPTION_PRINT_TEMPLATE_COULD_NOT_BE_DEPLOYED = "Print template could not be extracted";

}
